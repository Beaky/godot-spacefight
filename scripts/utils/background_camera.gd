class_name BackgroundCamera
extends Camera
# Camera for use in a 3D background.
# Follows the transform of the currently active camera of the root viewport.
# Currently, only uses the basis.


func _ready() -> void:
	_update_camera()


func _physics_process(_delta) -> void:
	_update_camera()


func _update_camera() -> void:
	var active_camera := (get_tree().get_root().get_camera() as Camera)
	if active_camera:
		global_transform.basis = active_camera.global_transform.basis
