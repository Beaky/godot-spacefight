extends AnimationPlayer
# Sync animations over the network of the Animation player this script is attached to


func _ready() -> void:
	set_process(false)
	if not MultiplayerHandler.is_singleplayer() and get_tree().is_network_server():
		connect("animation_started", self, "animation_started")
		connect("animation_finished", self, "animation_finished")

func _process(delta) -> void:
	rpc_unreliable("sync_progress", get_current_animation_position())


func animation_started(i) -> void:
	set_process(true)


func animation_finished(i) -> void:
	set_process(false)


slave func sync_progress(t) -> void:
	seek(t, true)
