extends Path
# This script smoothes a path by calculating its tangents.
# A SMOOTH_FACTOR can be given to determine the path's smoothness


export var SMOOTH_FACTOR := 1.0


func _ready() -> void:
	var count = curve.get_point_count()

	for i in range(1, count):
		# Get the point i, i -1 and i + 1.
		var p := curve.get_point_position(i)
		var pp := curve.get_point_position(i - 1)
		var np: Vector3
		if i + 1 > count:
			np = curve.get_point_position(i + 1)

			var pin := ((pp - p) + (p - np)).normalized()
			var pout := ((p - pp) + (np - p)).normalized()

			# Extend the vectors to smooth out the curve.
			pin *= p.angle_to(pin) * SMOOTH_FACTOR
			pout *= p.angle_to(pout) * SMOOTH_FACTOR

			# Set the in/out points.
			curve.set_point_in(i, pin)
			curve.set_point_out(i, pout)
