class_name SceneChanger
extends Node


"""
This class changes scenes smoothly and with a loading screen.
"""

export var default_scene: String
var interactive_loader: ResourceInteractiveLoader
var queued_scene: String

onready var scenes_root := $Scenes as Node
onready var loading_screen := $LoadingScreen as Control
onready var loading_progress := $LoadingScreen/ProgressBar as ProgressBar
onready var error_label := $LoadingScreen/Label as Label
onready var animplayer := $LoadingScreen/AnimationPlayer as AnimationPlayer


func _ready() -> void:
	set_process(false)
	loading_screen.visible = false
	connect_signals()
	if default_scene:
		call_deferred("change_scene", default_scene)


func _process(_delta: float) -> void:
	# Poll the interactive loader and add an error message to a label if it fails.
	var err := interactive_loader.poll()
	loading_progress.value = interactive_loader.get_stage()
	if err == ERR_FILE_EOF:
		set_process(false)
		change_scene_to(interactive_loader.get_resource())
		interactive_loader = null
	elif err != OK:
		error_label.set_text("ERROR: %s" % err)


func change_scene(scene_path: String) -> void:
	queued_scene = scene_path
	animplayer.play("fadein")
	loading_progress.value = 0


func _change_scene(scene_path: String) -> void:
		# Delete the last scene we were in
		if scenes_root.get_child_count() == 1:
			var last_scene = scenes_root.get_child(0)
			last_scene.queue_free()
		# Remember the old scene and remove it from the tree
		load_scene(scene_path)


func load_scene(scene_path: String) -> void:
	# Start loading the scene.
	interactive_loader = ResourceLoader.load_interactive(scene_path, "PackedScene")
	loading_progress.value = 0
	loading_progress.max_value = interactive_loader.get_stage_count()
	set_process(true)


func change_scene_to(scene: PackedScene) -> void:
	scenes_root.add_child(scene.instance())
	connect_signals()
	animplayer.play("fadeout")


func connect_signals() -> void:
	for node in get_tree().get_nodes_in_group("loading_areas"):
		node.connect("player_entered", self, "switch_scene")




func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if anim_name == "fadein":
		_change_scene(queued_scene)
