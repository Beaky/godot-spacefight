extends Particles
class_name ProjectileParticles
# Class for projectiles particles used in meshes to show missile trails or the like.
# Is NOT used for spark particles.
# Changes the emission state if the projectile is visible or not.
# This prevents the particles from showing when the projectile is reactivated.


func _ready() -> void:
	connect("visibility_changed", self, "_visibility_changed")


func _visibility_changed() -> void:
	restart()
	emitting = visible

