extends RayCast
class_name Projectile
# Base class for all projectiles. Acts as a bullet
# Each projectile is initialized by passing it a dictionary with all of its properties.
# This dictionary looks like this:
#{
#	# Movement and lifetime
#	"linear_velocity": 0.0,
#	"max_lifetime": 0.0,
#	"turn_speed": 0.0,	# Homing class only
#
#	# Explosive properties
#	"is_explosive": false,
#	"damage_falloff": 0.0,
#
#	# Proximity detection, only makes sense when is_explosive is set.
#	has_proximity_detection: false
#	proximity_distance: 0.0
#
#	# The actual damage dictionary
#	"damage": {
#		"type": "",
#		"damage": 0,
#		"effects": []
#	}
#}
signal active_changed

enum Type {
	PROJECTILE,
	HOMING,
	BEAM
}

var active := false setget set_active
var id: int

var source: Spatial # Where does this particle come from? This is set by the particle source.
var damage := {}

# Lifetime of the particles and the projectile itself
var MAX_LIFETIME := 2.0
var lifetime := 0.0
var is_dying := false


# Velocities of the projectile
var linear_velocity := 0.0

# Should this projectile behave like a dummy?
var is_dummy := false

# Should this projectile be explosive?
var is_explosive := false
var damage_distance := 0.0
var has_proximity_detection := false
var proximity_distance := 0.0

# Particles, initialized in _init
var spark_particles: Particles
var MAX_SPARK_PARTICLES_LIFETIME: float
var spark_particles_lifetime := 0.0

var model: Spatial
var model_mesh: MeshInstance

var follow_egress_node := false
var egress_node: Spatial


# Function to setup the projectile, this has to be called by the almighty creator of this projectile.
func _init(properties: Dictionary, lsource: Spatial) -> void:
	# Used for pooling later on.
	id = properties["id"]

	source = lsource
	cast_to = Vector3(0, 0, -1.5)

	linear_velocity = properties["linear_velocity"]
	if properties.has("follow_egress_node"):
		linear_velocity = 0.0
		follow_egress_node = properties["follow_egress_node"]
	MAX_LIFETIME = properties["max_lifetime"]

	if properties.has("is_explosive"):
		is_explosive = properties["is_explosive"]
		damage_distance = properties["damage_distance"]
		has_proximity_detection = properties["has_proximity_detection"]

	damage = properties["damage"]

	if not source or damage.empty():
		print("Some required values are not present, removing invalid bullet.")

	if is_explosive:
		var area := Area.new()
		area.connect("body_entered", self, "_on_Area_body_entered")
		add_child(area)

		var collision_shape := CollisionShape.new()
		area.add_child(collision_shape)

		var sphere_shape := SphereShape.new()
		sphere_shape.radius = damage_distance
		collision_shape.set_shape(sphere_shape)

	# Set up the model and particles
	model = properties["model_resource"].instance()
	add_child(model)
	model.visible = false
	model_mesh = model.get_node_or_null("MeshInstance")
	spark_particles = model.get_node_or_null("SparkParticles")
	if spark_particles:
		MAX_SPARK_PARTICLES_LIFETIME = spark_particles.lifetime


func _ready() -> void:
	if get_tree().has_network_peer() and not get_tree().is_network_server():
		is_dummy = true

	# Prevent source from inflicting damage onto itself
	add_exception(source)
	# Get particles node if available and the life time of the particles.
	# This is needed later when the bullet "dies"
	call_deferred("set_process", false)
	enabled = true


func _process(delta) -> void:
	if is_dying:
		spark_particles_lifetime += delta
		if spark_particles_lifetime >= MAX_SPARK_PARTICLES_LIFETIME:
			self.active = false
	elif lifetime > MAX_LIFETIME:
		spark()

	if follow_egress_node and is_instance_valid(egress_node):
		global_transform = egress_node.global_transform.orthonormalized()
		if not egress_node.is_visible_in_tree():
			spark()

	lifetime += delta
	move(delta)
	collide(delta)


# Simply move
func move(delta) -> void:
	global_transform.origin = global_transform.origin - global_transform.basis.z * linear_velocity * delta


func collide(_delta: float) -> void:
	var collider: PhysicsBody
	if is_colliding() and not is_dying:
		collider = get_collider()
	if is_instance_valid(collider):
		collider = (collider as Actor)
		if collider and not is_dummy:
			(collider as Actor).recieve_damage(damage, source)
		spark()


func _on_Area_body_entered(body) -> void:
	if not (is_dying or not active) and not body == source:
		if body.has_method("recieve_damage"):
			body.recieve_damage(damage, source)
		spark()


func spark() -> void:
	if spark_particles:
		enabled = false
		is_dying = true
		model_mesh.set_visible(false)
		spark_particles.emitting = true
	else:
		self.active = false


func set_active(val) -> void:
	if val:
		clear_exceptions()
		if source:
			add_exception(source)
		lifetime = 0.0
		spark_particles_lifetime = 0.0
		model.visible = true
		model_mesh.visible = true
		call_deferred("set_process", true)
		enabled = true
	else:
		enabled = false
		is_dying = false
		model.visible = false
		model_mesh.visible = false
		call_deferred("set_process", false)
	active = val
	emit_signal("active_changed", self, val)
