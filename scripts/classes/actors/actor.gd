extends KinematicBody
class_name Actor

# This is the class every actor inherits from.
# An actor is every entity that 'lives' in the game world.
# Living means that it is acting inside the game world and has health and can die.
# It also can fight.

signal recieved_damage
signal has_died
## Movement variables ##
var velocity := Vector3()

#	Does not actually affect movement but should be set if the actor is intended
#	to be stationary.
export (bool) var is_stationary := false
export (float) var MAX_HEALTH: float = 100.0
export (float) var MAX_SHIELDS: float = 100.0
export (float) var SHIELD_RECOVERY_DELAY := 4.0
export (float) var shield_recovery_speed := 20
export var MAX_VELOCITY := 0.0
export var MAX_STRAFE_VELOCITY := 0.0
export var weapon_node_path: NodePath # Can either be a Node Grouping weapons or a single weapon
export var turret_node_path: NodePath

var health := MAX_HEALTH
var shields := MAX_SHIELDS
var shield_recovery_timer: float
var is_dead := false
var target: Actor setget _set_target
var weapons := {}
var active_weapons_indeces := [] # Can have multiple wepons for multiple firing modes (primary/secondary)
var turrets := []

func _ready() -> void:
	add_to_group("actors")
# Workaround for godot not setting variables to their
# respective exported 'MAX_' values
	shields = MAX_SHIELDS
	health = MAX_HEALTH
	shield_recovery_timer = SHIELD_RECOVERY_DELAY
	_populate_weapons_dict(weapons, active_weapons_indeces)
	_populate_turret_array()


func _process(delta) -> void:
	# This process function for the most part only updates timers.
	if shields < MAX_SHIELDS and not MAX_SHIELDS == 0:
		shield_recovery_timer += delta
		if shield_recovery_timer >= SHIELD_RECOVERY_DELAY:
			shields += shield_recovery_speed * delta
			if shields >= MAX_SHIELDS:
				shield_recovery_timer = 0
				shields = MAX_SHIELDS
			update_health(health, shields)


func aim_turrets(to: Vector3) -> void:
	if MultiplayerHandler.is_singleplayer() or is_network_master():
		for turret in turrets:
			turret.aim_at(to)
			if not MultiplayerHandler.is_singleplayer() and is_network_master():
				turret.rpc_unreliable("aim_at", to)


puppet func set_puppet_transform_velocity(tform: Transform, vel: Vector3) -> void:
	global_transform = tform
	velocity = vel


func get_linear_velocity() -> Vector3:
	return velocity


## Health ##
func recieve_damage(dmg_dict: Dictionary, _source: Spatial) -> void:
	if MultiplayerHandler.is_singleplayer() or is_network_master():
		if shields > 0:
			shield_recovery_timer = 0
			shields -= dmg_dict["damage"]
			if shields < 0:
				shields = 0
		else:
			health -= dmg_dict["damage"]
		emit_signal("recieved_damage", self)
		if health <= 0:
			die()
		else:
			if not MultiplayerHandler.is_singleplayer():
				rpc("update_health", health, shields)


puppet func update_health(new_health: float, new_shields: float) -> void:
	if new_health < health or new_shields < shields:
		shield_recovery_timer = 0
	health = new_health
	shields = new_shields
	emit_signal("recieved_damage", self)


puppet func die() -> void:
	if is_dead:
		return
	set_physics_process(false)
	set_process(false)
	add_to_group("dead")
	is_dead = true
	set_visible(false)
	collision_layer = 0
	collision_mask = 0
	set_process(false)
	set_physics_process(false)
	emit_signal("has_died", self)
	if not MultiplayerHandler.is_singleplayer() and is_network_master():
		rpc("die")


## Tools ##
func calculate_preaim_pos(actor: Actor, bullet_speed: float, offset := Vector3.ZERO) -> Vector3:
	if bullet_speed == 0:
		return actor.global_transform.origin
	# http://howlingmoonsoftware.com/wordpress/leading-a-target/
	# Not entirely sure how they came up with the quadratic formula thing.
	var delta_pos := actor.global_transform.origin + offset - global_transform.origin
	var delta_vel := actor.get_linear_velocity() - get_linear_velocity()

	var a := delta_vel.dot(delta_vel) - pow(bullet_speed, 2)
	var b := 2 * delta_vel.dot(delta_pos)
	var c := delta_pos.dot(delta_pos)

	# Discriminant, not determinant
	var d := pow(b, 2) - 4.0 * a * c
	var time := 0.0
	if d > 0:
		# Simple quadratic formula solving
		time = 2*c / (sqrt(d) - b)
		return actor.global_transform.origin + offset + time * delta_vel
	else:
		return Vector3.ZERO


func _set_target(t: Actor) -> void:
	target = t
	if get_tree().has_network_peer() and is_network_master():
		rpc("_set_target_from_path", t.get_path())

puppet func _set_target_from_path(path: NodePath) -> void:
	target = get_node_or_null(path)


func cast_ray(from: Vector3, to: Vector3) -> Dictionary:
	var space_state = get_world().get_direct_space_state()
	var result = space_state.intersect_ray(from, to, [self], 1)
	return result


func get_active_weapon(firing_mode: int) -> Weapon:
	return weapons[firing_mode][active_weapons_indeces[firing_mode]] as Weapon


func _populate_weapons_dict(dict: Dictionary, active_weapon_indices: Array) -> void:
	for fire_mode in Weapon.FiringModes.values():
		dict[fire_mode] = []
		active_weapon_indices.append(0)
	var weapon_node := get_node_or_null(weapon_node_path)
	if not weapon_node:
		return

	if weapon_node is Weapon:
		weapon_node.source = self
		dict[weapon_node.firing_mode].append(weapon_node)
	else:
		for node in weapon_node.get_children():
			if node is Weapon:
				node.source = self
				dict[node.firing_mode].append(node)


func _populate_turret_array() -> void:
	var turret_node := get_node_or_null(turret_node_path)
	if not turret_node:
		return

	if turret_node is ActorControlledTurret:
		turrets.append(turret_node)
	else:
		for node in turret_node.get_children():
			if node is ActorControlledTurret:
				turrets.append(node)
