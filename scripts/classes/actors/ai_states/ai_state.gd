class_name AIState
extends Node


onready var parent = get_parent().get_parent()


func _ready() -> void:
	set_physics_process(false)


func _enter(_previous_State: AIState) -> void:
	pass


func _physics_process(_delta: float) -> void:
	pass


func _on_target_locked(_target: Actor) -> void:
	pass


func _on_target_unlocked(_target: Actor) -> void:
	pass


func _on_damage_received(_damage: Dictionary, _source) -> void:
	pass


func _exit() -> void:
	pass

func random_vector() -> Vector3:
	return Vector3(randf(), randf(), randf()).normalized()
