extends Node


signal player_registered
var peer: NetworkedMultiplayerENet
var players := {}
var local_info := {"name": "Shepard", "weapons": ["lars", "reaper"]}
var nuid: int
var is_server := false
var user_auth_queue = {}


func _ready() -> void:
	set_pause_mode(PAUSE_MODE_PROCESS)
	get_tree().connect("connected_to_server", self, "_connected_to_server")
	get_tree().connect("network_peer_disconnected", self, "player_disconnected")
	WebSocketHandler.connect("received_message", self, "handle_websocket_message")


func create_client(address, port) -> int:
	peer = NetworkedMultiplayerENet.new()
	var err = peer.create_client(address, port)
	if err == OK:
		get_tree().set_network_peer(peer)
		get_tree().set_meta("network_peer", peer)
	return err


func create_server(port=null) -> void:
	if not port:
		port = ConfigHandler.get_config("server", "port")

	# Only connect to the masterserver if we are a dedicated server.
	if (
		ConfigHandler.force_server or
		ConfigHandler.get_config("server", "start_as_server") or
		OS.has_feature("Server")
		):
		var token = ConfigHandler.get_config("server", "token")
		if not token:
			print("[ERROR] No authorization token provided!")
			return
		is_server = true
		WebSocketHandler.init_masterserver_connection()
		yield(WebSocketHandler.wsc, "connection_established")
		WebSocketHandler.login_server(token)
		WebSocketHandler.connect("websocket_authorized", self, "_authorized")


func _authorized() -> void:
	print("[INFO] Authorized to masterserver.")
	var port = ConfigHandler.get_config("server", "port")
	var err = _start_server(port)
	if err == OK:
		nuid = 1
		print("[INFO] Server started on port %s" % port)
	else:
		print("[ERROR] Recieved %s while creating server!" % err)


func _start_server(port: int) -> int:
	peer = NetworkedMultiplayerENet.new()
	var err = peer.create_server(port, 4)
	get_tree().set_network_peer(peer)
	get_tree().set_meta("network_peer", peer)
	return err


# Player asks the everybody to register them
func _connected_to_server() -> void:
	print("[INFO] Connected to server")
	# Hand our information over to the server
	rpc_id(1, "authorize_user", WebSocketHandler.client_info["username"], WebSocketHandler.client_info["session_token"])


remote func authorize_user(username: String, session_token: String) -> void:
	print("[INFO] Attempting to authorize user %s" % username)
	var sender_id = get_tree().get_rpc_sender_id()
	WebSocketHandler.send_message({
		"control": "request",
		"message": "is_user_logged_in",
		"data": {
			"username": username,
			"session_token": session_token
			}
		})
	user_auth_queue[username] = sender_id


remote func _register_self() -> void:
	if not get_tree().get_rpc_sender_id() == 1:
		return
	nuid = get_tree().get_network_unique_id()
	rpc_id(1, "register_player", get_tree().get_network_unique_id(), local_info)


# Everybody saves players information and updates their lobby.
remote func register_player(id: int, player_info: Dictionary) -> void:
	var rid = get_tree().get_rpc_sender_id()
	if not (id == rid or rid == 1):
		(get_tree().get_network_peer() as NetworkedMultiplayerENet).disconnect_peer(rid)

	players[id] = player_info
	players[id]["spawn_num"] = players.size() - 1
	# If we are the host, give the new player our welcomes
	if get_tree().is_network_server():
		for player in players.keys():
			# Send the information of the new player to the other players.
			rpc_id(player, "register_player", id, players[id])
			# Send the information of the other players to the new player.
			rpc_id(id, "register_player", player, players[player])
	print("[INFO] Player '{username}' registered with uid '{uid}'".format({"username": player_info["username"], "uid": id}))
	update_lobby(id)

	if get_tree().is_network_server():
		WebSocketHandler.update_server_info("idle")


func player_disconnected(id: int) -> void:
	players.erase(id)
	if players.size() == 0 and get_tree().is_network_server():
		reset()

func update_lobby(id: int) -> void:
	emit_signal("player_registered", id)


func reset() -> void:
	players = {}
	get_tree().set_pause(true)
	if ConfigHandler.get_config("server", "start_as_server") or OS.has_feature("Server"):
		WebSocketHandler.update_server_info("idle")
		print("[INFO] Reset server!")
		get_tree().change_scene("res://scenes/ui/multiplayer/hangar_lobby/hangar_lobby.tscn")
	else:
		get_tree().set_network_peer(null)
		get_tree().change_scene("res://scenes/ui/main_menu/main_menu.tscn")

	yield(get_tree(), "tree_changed")
	get_tree().set_refuse_new_network_connections(false)
	get_tree().set_pause(false)


func is_singleplayer() -> bool:
	return !get_tree().has_network_peer()


func handle_websocket_message(message) -> void:
	if message["control"] == "notification":

		if message["message"] == "is_user_logged_in":
			var msg_data = message["data"]
			if msg_data["logged_in"]:
				rpc_id(user_auth_queue[msg_data["username"]], "_register_self")
				user_auth_queue.erase(msg_data["username"])
				print("[INFO] Player %s successfully authorized." % msg_data["username"])
			else:
				(get_tree().get_network_peer() as NetworkedMultiplayerENet).disconnect_peer(user_auth_queue[msg_data["username"]])
				print("[WARNING] Player %s tried to connect without being logged into the masterserver" % msg_data["username"])
