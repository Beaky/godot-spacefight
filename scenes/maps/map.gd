class_name MapSceneRoot
extends Spatial
# This class handles some functionality related to multiplayer, like setting up players.
# It also takes note of where specific nodes are like the mission or the projectile pool.
# Also handles gravity


const player_res = preload("res://actors/ipc/player_ship/player.tscn")

export (float) var GRAVITY := 9.8
export (Vector3) var GRAVITY_VECTOR := Vector3(0, -1, 0)
export (bool) var instance_players := false
export (NodePath) var players_node_path: NodePath
export (NodePath) var local_player_path: NodePath

var players_node: Spatial
var players_ready = []

var local_player: PlayerShip

onready var mission := ($Mission as Mission)
onready var projectile_node: ProjectilePool = $ProjectilePool


func _ready() -> void:
	get_tree().paused = true

	# Check if players node exist where we instance the players into if we have to spawn them.
	if instance_players or not MultiplayerHandler.is_singleplayer():
		players_node = get_node_or_null(players_node_path)
		if not players_node:
			print("[ERROR] No valid NodePath to instance players")
			return

# Scene config
	# Set Gravity and the Gravity Vector
	PhysicsServer.area_set_param(
		get_world().get_space(),
		PhysicsServer.AREA_PARAM_GRAVITY,
		GRAVITY
		)
	PhysicsServer.area_set_param(
		get_world().get_space(),
		PhysicsServer.AREA_PARAM_GRAVITY_VECTOR,
		GRAVITY_VECTOR
		)
# Scene Config end

	# Don't capture the mouse if we're the server.
	if not (get_tree().has_network_peer() and get_tree().is_network_server()):
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

	# If we are in singleplayer do not instance players if it is not wanted.
	if MultiplayerHandler.is_singleplayer() and not instance_players:
		local_player = (get_node(local_player_path) as KinematicBody)

	# If we are in multiplayer, setup the players.
	if not MultiplayerHandler.is_singleplayer():
		print("[INFO] Network unique id: %s" % get_tree().get_network_unique_id())
		# If we are the server, setup the players
		if get_tree().is_network_server():
			get_tree().connect("network_peer_disconnected", self, "player_disconnected")
			rpc("setup_players")

	# If we're not even online, setup the single player.
	elif MultiplayerHandler.is_singleplayer():
		if instance_players and not local_player:
			var player := player_res.instance()
			local_player = player
			players_node.add_child(player)
			player.owner = self
			print("[INFO] Local player: " + str(local_player))
		# Init the mission system if this is a singleplayer game.
		# Multiplayer mission system is initiated later down the line.
		if mission:
			call_deferred("start_mission_system")
	if not mission:
		get_tree().paused = false
	if not local_player:
		print("[ERROR] No valid player found!")


remotesync func setup_players() -> void:
	print("[INFO] Setting up players")

	# For every player, create a new player entity.
	# The keys in the players dictionary are the unique ids.
	for p in MultiplayerHandler.players:
		var player = player_res.instance()
		player.set_name(str(p))
		# Set the players network master
		# player.set_network_master(p)
		if p == get_tree().get_network_unique_id():
			local_player = player
			player.get_node("camera_pivot/Camera").set_current(true)

		# Get a spawn point for us.
		var spawn_num: int = MultiplayerHandler.players[p]["spawn_num"]
		var spawn_node := (players_node.get_node("Spawn%s" % spawn_num) as Position3D)
		var spawn_position := spawn_node.get_translation()
		player.set_translation(spawn_position)
		players_node.add_child(player)
		player.owner = self

	# Delete left over spawn position
	for child in players_node.get_children():
		if child is Position3D:
			child.queue_free()

	yield(get_tree().create_timer(0.5), "timeout")
	if not is_network_master():
		rpc_id(1, "done_preconfiguring", get_tree().get_network_unique_id())


remote func done_preconfiguring(id) -> void:
	print("[INFO] Player %s is ready" % id)
	players_ready.append(id)
	if players_ready.size() == MultiplayerHandler.players.size() and get_tree().is_network_server():
		# Wait a bit
		print("[WAITING]")
		yield(get_tree().create_timer(2.0), "timeout")

		print("[INFO] All players are ready!")
		if mission:
			rpc("start_mission_system")


remotesync func start_mission_system() -> void:
	get_tree().paused = false
	mission.init_mission(local_player)


func player_disconnected(id) -> void:
	var player := players_node.get_node(str(id))
	if is_instance_valid(player):
		player.queue_free()
